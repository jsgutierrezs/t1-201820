package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	public int sumatory(IntegersBag bag) {
		int sum = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				sum += iter.next();
			}
		}
		return sum;
	}
	
	public int substraction(IntegersBag bag) {
		int sub = Integer.MAX_VALUE;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				sub += iter.next();
			}
		}
		return sub;
	}
	
	public int multiply(IntegersBag bag) {
		int multi = 1;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				multi += iter.next();
			}
		}
		return multi;
	}
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
}
