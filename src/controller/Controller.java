package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	public static int sumatory(IntegersBag bag) {
	    return model.sumatory(bag);
	}
	
	public static int substraction(IntegersBag bag) {
	    return model.substraction(bag);
	}
	
	public static int multiply(IntegersBag bag) {
	    return model.multiply(bag);
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
}
